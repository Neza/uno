from tkinter import *

#### BARVE ####
MODRA = 'modra'
ZELENA = 'zelena'
RDECA = 'rdeča'
RUMENA = 'rumena'

class IzbiraBarve(Toplevel):
    """Izberi eno od stirih barv."""

    def __init__(self, parent):
        """Vzpostavitev dialoga.
        Argument: parent -- parent window (the application window)"""

        Toplevel.__init__(self, parent)

        self.withdraw() # ostane nevidno
        # if the master is not viewable, don't
        # make the child transient, or else it
        # would be opened withdrawn
        if parent.winfo_viewable():
            self.transient(parent)

        self.title('Izberi barvo')

        self.parent = parent

        self.barva = None

        body = Frame(self)

        body.pack(padx=5, pady=5)

        self.buttonbox()

        self.protocol("WM_DELETE_WINDOW", self.cancel)

        if self.parent is not None:
            self.geometry("+%d+%d" % (parent.winfo_rootx()+150,
                                      parent.winfo_rooty()+150))

        self.deiconify() # postane vidno

        self.focus_set()

        # pocakamo, da se okno pojavi na zaslonu preden poklicemo grab_set
        self.wait_visibility()
        self.grab_set()
        self.wait_window(self)


    def destroy(self):
        """Uničimo okno."""
        self.initial_focus = None
        Toplevel.destroy(self)


    def buttonbox(self):
        """Ustvarili bomo buttonbox, kamor po potrebi dodajamo gumbe."""
        box = Frame(self)

        w = Button(box, text=RDECA, width=10, bg='red', font=('Courier'), command=self.rdeca, default=ACTIVE)
        w.pack(side=LEFT, padx=5, pady=5)
        w = Button(box, text=ZELENA, width=10, bg='green', font=('Courier'), command=self.zelena, default=ACTIVE)
        w.pack(side=LEFT, padx=5, pady=5)
        w = Button(box, text=RUMENA, width=10, bg='yellow', font=('Courier'), command=self.rumena, default=ACTIVE)
        w.pack(side=LEFT, padx=5, pady=5)
        w = Button(box, text=MODRA, width=10, bg='blue', font=('Courier'), command=self.modra, default=ACTIVE)
        w.pack(side=LEFT, padx=5, pady=5)

        box.pack()


    # standardna semantika gumbov
    def rdeca(self, event=None):
        self.withdraw()
        self.update_idletasks()
        self.barva = RDECA
        self.parent.focus_set()
        self.destroy()

    def rumena(self, event=None):
        self.withdraw()
        self.update_idletasks()
        self.barva = RUMENA
        self.parent.focus_set()
        self.destroy()

    def modra(self, event=None):
        self.withdraw()
        self.update_idletasks()
        self.barva = MODRA
        self.parent.focus_set()
        self.destroy()

    def zelena(self, event=None):
        self.withdraw()
        self.update_idletasks()
        self.barva = ZELENA
        self.parent.focus_set()
        self.destroy()

    def cancel(self, event=None):
        # vrnemo fokus nazaj na glavno okno (parent window)
        if self.parent is not None:
            self.parent.focus_set()
        self.destroy()
