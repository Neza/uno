# README #


### Čemu je ta repozitorij namenjen? ###
* Projektu igrica pri predmetu PROG 2.
* Moj projekt: Uno

### Kratek opis projekta ###
Igrica [Uno] je pri nas bolj poznana tudi kot igra "Enka". Igralec igra proti trem računalniškim igralcem. Zmaga tisti, ki se prvi znebi vseh svojih kart. V skupnem seštevku je boljši tisti, ki ima manj točk.
Pravila bodo morda malček prilagojena, saj vemo, da obstaja kar nekaj neskladnosti glede samega igranja. Prav tako bomo morda imeli kakšno drugačno različico točkovanja kart, kot ste je vajeni.

Če želite igrati, enostavno poženete datoteko `uno.py`. :)

### Na koga se lahko obrnete za več informacij? ###
Neža Dimec (neza.dimec@gmail.com ali neza.dimec@student.fmf.uni-lj.si)

![unoLogo-1.png](https://bitbucket.org/repo/B86Aqz/images/1368332514-unoLogo-1.png)

[Uno]:http://en.wikipedia.org/wiki/Uno_%28card_game%29