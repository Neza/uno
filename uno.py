__author__ = 'NežaD'

from random import *
from tkinter import *
import dialog

#### ~.Igro bomo napisali za 4 igralce: 1 clovek, 3 racunalniki.~ ####


#### BARVE & TIPI KART ####
MODRA = 'modra'
ZELENA = 'zelena'
RDECA = 'rdeča'
RUMENA = 'rumena'
CRNA = 'črna'

STOP = 'stop'
OBRNI = 'obrni smer'
VZEMI2 = 'vzemi 2'
VZEMI4 = 'vzemi 4'
MENJAJ = 'menjaj barvo'

#### IGRALCI ####
RACUNALNIK1 = 'Računalnik 1'
RACUNALNIK2 = 'Računalnik 2'
RACUNALNIK3 = 'Računalnik 3'
IGRALEC = 'Igralec'


class Karta:
    """Razred karta."""
    
    def __init__(self, barva, tip, oznaka=None, vrednost=None):
        self.barva = barva
        self.tip = tip
        self.slika = None           # sem bomo shranili sliko karte
        self.oznaka = oznaka        # v oznako bomo shranili id karte, ki smo jo narisali
        self.vrednost = vrednost    # potrebovali bomo za sestevek tock

    def __str__(self):
        if self.tip != None:
            return (self.barva + ' ' + self.tip)
        return (self.barva + ' None')

    def __eq__(self, other):
        return self.barva == other.barva and self.tip == other.tip

    def __lt__(self, other):
        if (self.barva, self.tip) < (other.barva, other.tip):
            return True
        if other.tip == None or self.tip == None:
            return True
        return False

    def primerna(self, other):
        if other.tip == None or self.tip == None:
            if self.barva == other.barva or self.barva == CRNA:
                return True
        if self.barva == other.barva  or self.tip == other.tip or self.barva == CRNA:
            return True
        return False

    def narisi(self, platno, slika, x, y):
        self.slika = slika
        self.oznaka = platno.create_image(x, y, image=self.slika)
        platno.update_idletasks() # karta se takoj izrise

    def zbrisi(self, platno):
        platno.delete(self.oznaka)
        self.oznaka = None      


        

class Deck:
    """Pomozni razred za deljenje kart etc."""
    
    def __init__(self, shuffled = True):
        self.karte = []
        barva1 = [RUMENA, MODRA, ZELENA, RDECA]
        tip1 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', STOP, OBRNI, VZEMI2]
        barva2 = [CRNA]
        tip2 = [MENJAJ, VZEMI4]

        for barva in barva1:
            for tip in tip1:
                if tip == '0':
                    self.karte.append(Karta(barva, tip, None, 0))
                else:
                    for i in range(2):
                        if tip == STOP or tip == OBRNI:
                            self.karte.append(Karta(barva, tip, None, 10))
                        elif tip == VZEMI2:
                            self.karte.append(Karta(barva, tip, None, 20))
                        else:
                            self.karte.append(Karta(barva, tip, None, int(tip)))

        for barva in barva2:
            for tip in tip2:
                if tip == VZEMI4:
                    for i in range(4):
                        self.karte.append(Karta(barva, tip, None, 40))
                elif tip == MENJAJ:
                    for i in range(8):
                        self.karte.append(Karta(barva, tip, None, 40))
                        
        if shuffled:
            self.premesaj()
                        
    def print_deck(self):
        """Metoda, s katero si pogledam vse karte."""
        for karta in self.karte:
            print(karta)

    def __str__(self):
        """Diagonalni izpis kart (izkljucno zaradi preglednosti kart v posameznih rokah) ..."""
        s = ""
        for i in range(len(self.karte)):
            s = s + " " * i + str(self.karte[i]) + "\n"
        return s
        # deck = Deck(), potem pa dam print(deck)

    def premesaj(self):
        """Premesa karte."""
        shuffle(self.karte)

    def odstrani(self, karta):
        """Odstrani karto."""
        if karta in self.karte:
            self.karte.remove(karta)
            return True
        else:
            return False

    def sort(self):
        """Z metodo sort bomo posortirali karte, ki jih ima igralec v roki."""
        self.karte.sort(key=lambda karta: (karta.barva, karta.tip))
        return self.karte

    def je_prazno(self):
        """Fino je vedeti, da nam je v kupcku zmanjkalo kart."""
        return len(self.karte) == 0

    def razdeli(self, hands, st_kart = 0):
        # hands je slovar igralcev, ki jim bomo razdelili karte
        # st_kart pove, koliko kart bomo razdelili; ce nic ne povemo, torej ne bomo nic delili
        st_hands = len(hands)                   # toliko igralcev imamo
        igralci = list(hands.keys())
        for i in range(st_kart):
            if self.je_prazno():
                break                           # koncaj, ce nimamo dovolj kart
            card = self.karte.pop()             # vzemi zadnjo karto
            hand = igralci[i % st_hands]        # komu naslednjemu dam karto? eno po eno vsakemu posebej
            hands[hand].append(card)            # dodaj karto v roko



  
class CardGame():
    """Igra."""

    def __init__(self, root, n = 4):
        self.root = root
        self.after_id = None
        self.odlozene = []              # karte, ki padejo na odlagalni kupcek
        self.slike = []                 # slike kart, ki jih se ima v roki igralec
        self.slike_rac = []
        self.deck = Deck()              # tukaj noter imam vse karte
        self.igralci = []               # seznam igralcev, kjer bomo določili, kdo igra prvi itd.
        self.hand = dict()              # slovar vseh igralcev in njihovih kart
        self.zadnja_karta = None        # da vemo, katera je zadnja karta, ki je padla na odlagalni kupcek
        self.igralec_na_vrsti = None    # na potezi
        self.vzel_karto = False         # pomozna spremenljivka za klikanje na kupcek
        self.var = BooleanVar(self.root)
        self.panika = False
        self.poteza = False             # preverjamo, ali je igralec odlozil kaksno karto ali ne
        self.slike_rac1 = []
        self.slike_rac2 = []
        self.slike_rac3 = []
        self.tekst = []
        self.slikica =[]

        
        #### GUI (graficni vmesnik) ####
        self.root.title('Uno')

        # menu        
        menu = Menu(self.root)
        master.config(menu = menu) #dodamo menu

        igra_menu = Menu(menu)
        menu.add_cascade(label="Igra", menu=igra_menu)
        igra_menu.add_command(label="Nova igra", underline=0,
                              command = self.nova_igra)
        igra_menu.add_separator()
        igra_menu.add_command(label="Izhod", underline=0,
                              command = self.izhod)
        
        # OKVIR
        self.okvir = Frame(self.root, borderwidt=5)
        self.okvir.grid(row=0, column=0)

        # gumb nova igra
        self.nova = Button(self.okvir, text='Nova igra', width=12, font=('Courier'), command=self.gumb_nova_igra, state=ACTIVE)
        self.nova.grid(row=3, column=0)
        
        # gumb naprej, v primeru da igralec nima v roki nicesar, kar bi hotel odvreci na kupcek
        self.naprej = Button(self.okvir, text='Naprej', width=12, font=('Courier'), command=self.gumb_naprej, state=DISABLED)
        self.naprej.grid(row=3, column=1,padx=2)

        # statistika
        self.okvir_statistika = LabelFrame(self.okvir, text='STATISTIKA:', font=('Batang', 10, 'bold'), fg='#52729E')
        self.okvir_statistika.grid(row=3, column=2)
        
        self.rac1 = IntVar()
        self.rac1t = 0
        self.rac1.set(self.rac1t)
        self.rac2 = IntVar()
        self.rac2t = 0
        self.rac2.set(self.rac2t)
        self.rac3 = IntVar()
        self.rac3t = 0
        self.rac3.set(self.rac3t)
        self.igr = IntVar()
        self.igrt = 0
        self.igr.set(self.igrt)

        self.p = Label(self.okvir_statistika, text='Računalnik 1:', font=('Batang', 10)).grid(row=0, column=0)
        self.prvi = Label(self.okvir_statistika, textvariable=self.rac1, font=('Batang', 10)).grid(row=0, column=1)
        self.d = Label(self.okvir_statistika, text='Računalnik 2:', font=('Batang', 10)).grid(row=0, column=2)
        self.drugi = Label(self.okvir_statistika, textvariable=self.rac2, font=('Batang', 10)).grid(row=0, column=3)
        self.t = Label(self.okvir_statistika, text='Računalnik 3:', font=('Batang', 10)).grid(row=0, column=4)
        self.tretji = Label(self.okvir_statistika, textvariable=self.rac3, font=('Batang', 10)).grid(row=0, column=5)
        self.j = Label(self.okvir_statistika, text='Igralec:', font=('Batang', 10)).grid(row=0, column=6)
        self.jaz = Label(self.okvir_statistika, textvariable=self.igr, font=('Batang', 10)).grid(row=0, column=7)

        self.o = IntVar() 
        self.stevilo_iger = 0 # stevec odigranih iger
        self.o.set(self.stevilo_iger)
        self.odigrane_tekst = Label(self.okvir_statistika, text='Število odigranih iger:', font=('Batang', 10)).grid(row=0, column=8)
        self.odigrane = Label(self.okvir_statistika, textvariable=self.o, font=('Batang', 10)).grid(row=0, column=9)
        
        # obmocje za risanje
        self.canvas = Canvas(self.okvir, width=790, height=650, bg='#52729E')
        self.canvas.grid(row=0, column=0, columnspan=3, padx=5, pady=5)

        self.s = PhotoImage(file='./pozdrav.gif')
        self.pozdrav = self.canvas.create_image(300, 300, image=self.s)

        # pozdravni napisi :)
        self.w = 'Dobrodošel/-a v igri UNO!\nZa začetek igre klikni na gumb \'Nova igra\' spodaj.' 
        self.welcome = self.canvas.create_text(350, 550, text=self.w, fill='white', font=('Batang', 18))
        
        self.p = 'Na vrsti je igralec, pri katerem se pojavi TrollFace. :)'
        self.pojasnilo = self.canvas.create_text(350, 600, text=self.p, fill='white', font=('Batang', 16))
        
        # slovar slik za karte
        self.slovar_slik = {(MODRA, '0'): PhotoImage(file='./Karte/blue_0.gif'),
          (MODRA, '1'): PhotoImage(file='./Karte/blue_1.gif'),
          (MODRA, '2'): PhotoImage(file='./Karte/blue_2.gif'),
          (MODRA, '3'): PhotoImage(file='./Karte/blue_3.gif'),
          (MODRA, '4'): PhotoImage(file='./Karte/blue_4.gif'),
          (MODRA, '5'): PhotoImage(file='./Karte/blue_5.gif'),
          (MODRA, '6'): PhotoImage(file='./Karte/blue_6.gif'),
          (MODRA, '7'): PhotoImage(file='./Karte/blue_7.gif'),
          (MODRA, '8'): PhotoImage(file='./Karte/blue_8.gif'),
          (MODRA, '9'): PhotoImage(file='./Karte/blue_9.gif'),
          (MODRA, OBRNI): PhotoImage(file='./Karte/blue_change.gif'),
          (MODRA, VZEMI2): PhotoImage(file='./Karte/blue_plus2.gif'),
          (MODRA, STOP): PhotoImage(file='./Karte/blue_stop.gif'),
          (ZELENA, '0'): PhotoImage(file='./Karte/green_0.gif'),
          (ZELENA, '1'): PhotoImage(file='./Karte/green_1.gif'),
          (ZELENA, '2'): PhotoImage(file='./Karte/green_2.gif'),
          (ZELENA, '3'): PhotoImage(file='./Karte/green_3.gif'),
          (ZELENA, '4'): PhotoImage(file='./Karte/green_4.gif'),
          (ZELENA, '5'): PhotoImage(file='./Karte/green_5.gif'),
          (ZELENA, '6'): PhotoImage(file='./Karte/green_6.gif'),
          (ZELENA, '7'): PhotoImage(file='./Karte/green_7.gif'),
          (ZELENA, '8'): PhotoImage(file='./Karte/green_8.gif'),
          (ZELENA, '9'): PhotoImage(file='./Karte/green_9.gif'),
          (ZELENA, OBRNI): PhotoImage(file='./Karte/green_change.gif'),
          (ZELENA, VZEMI2): PhotoImage(file='./Karte/green_plus2.gif'),
          (ZELENA, STOP): PhotoImage(file='./Karte/green_stop.gif'),
          (RDECA, '0'): PhotoImage(file='./Karte/red_0.gif'),
          (RDECA, '1'): PhotoImage(file='./Karte/red_1.gif'),
          (RDECA, '2'): PhotoImage(file='./Karte/red_2.gif'),
          (RDECA, '3'): PhotoImage(file='./Karte/red_3.gif'),
          (RDECA, '4'): PhotoImage(file='./Karte/red_4.gif'),
          (RDECA, '5'): PhotoImage(file='./Karte/red_5.gif'),
          (RDECA, '6'): PhotoImage(file='./Karte/red_6.gif'),
          (RDECA, '7'): PhotoImage(file='./Karte/red_7.gif'),
          (RDECA, '8'): PhotoImage(file='./Karte/red_8.gif'),
          (RDECA, '9'): PhotoImage(file='./Karte/red_9.gif'),
          (RDECA, OBRNI): PhotoImage(file='./Karte/red_change.gif'),
          (RDECA, VZEMI2): PhotoImage(file='./Karte/red_plus2.gif'),
          (RDECA, STOP): PhotoImage(file='./Karte/red_stop.gif'),
          (RUMENA, '0'): PhotoImage(file='./Karte/yellow_0.gif'),
          (RUMENA, '1'): PhotoImage(file='./Karte/yellow_1.gif'),
          (RUMENA, '2'): PhotoImage(file='./Karte/yellow_2.gif'),
          (RUMENA, '3'): PhotoImage(file='./Karte/yellow_3.gif'),
          (RUMENA, '4'): PhotoImage(file='./Karte/yellow_4.gif'),
          (RUMENA, '5'): PhotoImage(file='./Karte/yellow_5.gif'),
          (RUMENA, '6'): PhotoImage(file='./Karte/yellow_6.gif'),
          (RUMENA, '7'): PhotoImage(file='./Karte/yellow_7.gif'),
          (RUMENA, '8'): PhotoImage(file='./Karte/yellow_8.gif'),
          (RUMENA, '9'): PhotoImage(file='./Karte/yellow_9.gif'),
          (RUMENA, OBRNI): PhotoImage(file='./Karte/yellow_change.gif'),
          (RUMENA, VZEMI2): PhotoImage(file='./Karte/yellow_plus2.gif'),
          (RUMENA, STOP): PhotoImage(file='./Karte/yellow_stop.gif'),
          (CRNA, MENJAJ): PhotoImage(file='./Karte/change_colour.gif'),
          (CRNA, VZEMI4): PhotoImage(file='./Karte/plus_4.gif')}


    def izhod(self):
        if self.after_id != None:
            self.canvas.after_cancel(self.after_id)
        self.after_id = None
        self.var.set(True)
        self.root.destroy()
            
    def naslednji_igralec(self, igralec, korak=1):
        """Izracun naslednjega igralca na vrsti."""
        indeks = (self.igralci.index(self.igralec_na_vrsti) + korak) % len(self.igralci)
        self.igralec_na_vrsti = self.igralci[indeks]
        return self.igralci[indeks]

    def imamo_zmagovalca(self, igralec):
        """Ko igralec naredi potezo, bomo preverili, ali je morda zmagal."""
        return len(self.hand[igralec]) == 0

    def gumb_naprej(self):
        """Metoda, ki se izvede ob kliku na gumb naprej."""
        self.var.set(True)
        print('igralec je kliknil naprej')

    def gumb_nova_igra(self):
        """Metoda, ki se izvede ob kliku na gumb za novo igro."""
        self.nova_igra()

    def igraj_karto(self, igralec, karta):
        """Kar se zgodi, ko igralec igra doloceno mocnejso karto."""
        indeks = (self.igralci.index(self.igralec_na_vrsti) + 1) % len(self.igralci)
        naslednji = self.igralci[indeks]
        if karta.tip == VZEMI2:
            for i in range(2):
                karta = self.deck.karte.pop()
                self.hand[naslednji].append(karta)
                if naslednji == IGRALEC:
                    self.slike.append(karta)
                    karta.narisi(self.canvas, self.slovar_slik[(karta.barva, karta.tip)], (len(self.slike) - 1)*35 + 150, 600) 
                if naslednji == RACUNALNIK1:
                    self.vod = self.canvas.create_image(62, 500 - (len(self.hand[RACUNALNIK1]) - 1)*35, image=self.vodoravna)
                    self.slike_rac1.append(self.vod)
                if naslednji == RACUNALNIK2:
                    self.nav = self.canvas.create_image(150 + (len(self.hand[RACUNALNIK2]) - 1)*35, 75, image=self.navpicna)
                    self.slike_rac2.append(self.nav)
                if naslednji == RACUNALNIK3:
                    self.vod2 = self.canvas.create_image(730, 157 + (len(self.hand[RACUNALNIK3]) - 1)*35, image=self.vodoravna2)
                    self.slike_rac3.append(self.vod2)
            return
        if karta.tip == VZEMI4:
            for i in range(4):
                karta = self.deck.karte.pop()
                self.hand[naslednji].append(karta)
                if naslednji == IGRALEC:
                    self.slike.append(karta)
                    karta.narisi(self.canvas, self.slovar_slik[(karta.barva, karta.tip)], (len(self.slike) - 1)*35 + 150, 600)
                if naslednji == RACUNALNIK1:
                    self.vod = self.canvas.create_image(62, 500 - (len(self.hand[RACUNALNIK1]) - 1)*35, image=self.vodoravna)
                    self.slike_rac1.append(self.vod)
                if naslednji == RACUNALNIK2:
                    self.nav = self.canvas.create_image(150 + (len(self.hand[RACUNALNIK2]) - 1)*35, 75, image=self.navpicna)
                    self.slike_rac2.append(self.nav)
                if naslednji == RACUNALNIK3:
                    self.vod2 = self.canvas.create_image(730, 157 + (len(self.hand[RACUNALNIK3]) - 1)*35, image=self.vodoravna2)
                    self.slike_rac3.append(self.vod2)
            return
        if karta.tip == OBRNI:
            self.igralci = self.igralci[::-1]
            return
            

    #### komunikacija s cloveskim igralcem ####
    def klik(self, event):
        # klik na kupcek za vlecenje kart
        if self.vzel_karto == False:    
            if 263 <= event.y <= 337 and 375 <= event.x <= 424:
                self.naprej.config(state=ACTIVE)
                karta = self.deck.karte.pop()
                self.hand[IGRALEC].append(karta)
                self.slike.append(karta)
                karta.narisi(self.canvas, self.slovar_slik[(karta.barva, karta.tip)], (len(self.slike) - 1)*35 + 150, 600)
                self.vzel_karto = True

        #klik na karto v roki
        if 560 <= event.y <= 637 and 125 <= event.x <= len(self.slike) * 35 + 125 + 15:
            indeks_karte = min((event.x - 125)//35, len(self.slike) - 1)
            if self.slike[indeks_karte].primerna(self.zadnja_karta):
                self.canvas.coords(self.slike[indeks_karte].oznaka, 300, 300)
                self.canvas.tag_raise(self.slike[indeks_karte].oznaka, self.odlozene[-1].oznaka)
                self.igraj_karto(IGRALEC, self.slike[indeks_karte])
                self.zadnja_karta = self.slike[indeks_karte]
                print('igralec je odvrgel {}'.format(self.slike[indeks_karte]))

                # pojavi se okno za izbiro barve za nadaljevanje igre
                if self.slike[indeks_karte].barva == CRNA:
                    d = dialog.IzbiraBarve(self.root)
                    self.zadnja_karta.barva = d.barva
                    self.zadnja_karta.tip = self.slike[indeks_karte].tip # na to nobena karta ne bo mogla pasat gor; ce slucajno bo ujemanje v tipu, potem bo isto crna, torej ni problema
                    
                    print('izbrali smo barvo {}'.format(self.zadnja_karta.barva))

                self.hand[IGRALEC].remove(self.slike[indeks_karte])
                self.odlozene.append(self.slike[indeks_karte])
                self.slike.pop(indeks_karte)

                # premik levo za karte, ki so igralcu ostale v roki
                for karta in self.slike[indeks_karte:]:
                    self.canvas.move(karta.oznaka, -35, 0)
                
                self.var.set(True)
                self.poteza = True
                
    def klikni_potezo(self):
        """Poteza cloveka."""
        self.canvas.bind('<Button-1>', self.klik)
        self.var.set(False)
        self.canvas.wait_variable(self.var)
        self.canvas.unbind('<Button-1>')


    #### AI ####
    def naredi_potezo(self, igralec):
        """Poteza racunalniskega igralca."""
        # ker imamo najverjetneje nekje cudne nastavitve zadnje karte ali pa odvrzene, moramo narediti tole
        # (razlog: tece nam voda v grlo, ker se je bug pojavil na zadnji dan za oddajo igrice)
        for karta in self.hand[igralec]:
            if karta.tip == MENJAJ or karta.tip == VZEMI4:
                if karta.barva != CRNA:
##                    print('tole je bila nepravilna karta: {}---------------', karta)
                    self.hand[igralec].append(Karta(CRNA, karta.tip))
                    self.hand[igralec].remove(karta)
        
        izbrali = False # karte, ki jo bomo odvrgli, se nismo izbrali
        ustrezne = [karta for karta in self.hand[igralec] if karta.primerna(self.zadnja_karta)]
        self.odvrzena = None
                    
        if len(ustrezne) == 0:
            nova = self.deck.karte.pop()
            self.hand[igralec].append(nova)

            if self.igralec_na_vrsti == RACUNALNIK1:
                self.vod = self.canvas.create_image(62, 500 - (len(self.hand[RACUNALNIK1]) - 1)*35, image=self.vodoravna)
                self.slike_rac1.append(self.vod)
            if self.igralec_na_vrsti == RACUNALNIK2:
                self.nav = self.canvas.create_image(150 + (len(self.hand[RACUNALNIK2]) - 1)*35, 75, image=self.navpicna)
                self.slike_rac2.append(self.nav)
            if self.igralec_na_vrsti == RACUNALNIK3:
                self.vod2 = self.canvas.create_image(730, 157 + (len(self.hand[RACUNALNIK3]) - 1)*35, image=self.vodoravna2)
                self.slike_rac3.append(self.vod2)
                
            if nova.primerna(self.zadnja_karta):
                ustrezne.append(nova)

        if len(ustrezne) != 0: # ne sme biti else!
            # preverimo, ali ima igralec pred nami v roki morda samo eno karto
            indeks = (self.igralci.index(self.igralec_na_vrsti) + 1) % len(self.igralci)
            naslednji = self.igralci[indeks]
            
            if len(self.hand[naslednji]) == 1:
                print('**********panika********** {} ima samo eno karto: UNO!!!'.format(naslednji))
                zaustavitvene = self.zaustavitvene_karte(ustrezne)
                if len(zaustavitvene) != 0:
                    print(zaustavitvene)
                    self.odvrzena = zaustavitvene[-1]
                    self.panika = True
                    # poiskali smo najboljso mozno karto za napad na uno-ta pred nami :)
                            
            # izvedemo, ce nismo v paniki, saj igralec pred nami nima v roki samo ene karte
            # in izvedemo, ce nimamo nobene pametne zaustavitvene karte :(
            if not self.panika:
                posortirane = self.barvno_sortiranje(ustrezne)
                if self.zadnja_karta.barva == MODRA:
                    if len(posortirane[0]) != 0:
                        self.odvrzena = posortirane[0][-1]
                        izbrali = True
                if self.zadnja_karta.barva == RDECA:
                    if len(posortirane[1]) != 0:
                        self.odvrzena = posortirane[1][-1]
                        izbrali = True
                if self.zadnja_karta.barva == RUMENA:
                    if len(posortirane[2]) != 0:
                        self.odvrzena = posortirane[2][-1]
                        izbrali = True
                if self.zadnja_karta.barva == ZELENA:
                    if len(posortirane[3]) != 0:
                        self.odvrzena = posortirane[3][-1]
                        izbrali = True
                if izbrali == False:
                    if len(posortirane[4]) != 0:
                        self.odvrzena = posortirane[4][-1]
                    else: # ce karta ni iste barve kot zadnja karta, potem je vseeno katero damo gor, saj imajo ustrezne isti tip kot zadnja
                        self.odvrzena = choice(ustrezne)
                        
            if self.igralec_na_vrsti == RACUNALNIK1:
                self.canvas.delete(self.slike_rac1.pop())
            if self.igralec_na_vrsti == RACUNALNIK2:
                self.canvas.delete(self.slike_rac2.pop())
            if self.igralec_na_vrsti == RACUNALNIK3:
                self.canvas.delete(self.slike_rac3.pop())

            self.hand[igralec].remove(self.odvrzena)
            
            # self.odvrzena = choice(ustrezne) # uporabimo ce zelimo imeti neumne racunalniske igralce, ki random odmetujejo ustrezne karte
            print('{} je odvrgel {}'.format(self.igralec_na_vrsti, self.odvrzena))
            self.odvrzena.narisi(self.canvas, self.slovar_slik[(self.odvrzena.barva, self.odvrzena.tip)], 300, 300)
            self.odlozene.append(self.odvrzena)
            self.igraj_karto(igralec, self.odvrzena)
                
            if self.odvrzena.barva == CRNA:
                self.zadnja_karta = self.odvrzena
                if self.panika:
                    # izbere drugo barvo kot je bila pred tem na odlagalnem kupcku
                    optimalne_barve = [MODRA, RDECA, RUMENA, ZELENA]
                    if self.zadnja_karta.barva != CRNA:
                        optimalne_barve.remove(self.zadnja_karta.barva)
                    self.zadnja_karta.barva = choice(optimalne_barve)
                else:
                    # izbere tisto barvo, ki je ima najvec v roki
                    self.zadnja_karta.barva = self.izberi_barvo(self.hand[igralec])
            else:
                self.zadnja_karta = self.odvrzena

            self.poteza = True
            
        else:
            self.poteza = False
            print('{} nima nobene ustrezne karte'.format(self.igralec_na_vrsti))

        self.panika = False
        
    # pomozne metode za boljso strategijo igranja racunalnikov
    def barvno_sortiranje(self, roka):
        """Metoda s katero bomo posortirali karte tako, da bomo dobili vrnjen tuple:
        ([MODRE karte], [RDECE karte], [RUMENE karte], [ZELENE karte], [CRNE karte]).
        Seznami so urejeni: stevilke narascajoce, sledijo: obrni, stop, vzemi2."""
        modre = sorted([karta for karta in roka if karta.barva == MODRA])
        rdece = sorted([karta for karta in roka if karta.barva == RDECA])
        rumene = sorted([karta for karta in roka if karta.barva == RUMENA])
        zelene = sorted([karta for karta in roka if karta.barva == ZELENA])
        crne = sorted([karta for karta in roka if karta.barva == CRNA])
        return (modre, rdece, rumene, zelene, crne)

    def izberi_barvo(self, roka):
        """Metoda, ki bo pomagala AI, da izbere barvo,
        ko odvrze karto, ki zahteva menjavo barve za nadaljevanje igre."""
        modre = [karta for karta in roka if karta.barva == MODRA]
        rdece = [karta for karta in roka if karta.barva == RDECA]
        rumene = [karta for karta in roka if karta.barva == RUMENA]
        zelene = [karta for karta in roka if karta.barva == ZELENA]
        maksi = max(len(modre), len(rdece), len(rumene), len(zelene))
        s = [sez for sez in [modre, rdece, rumene, zelene] if len(sez) == maksi]
        vrni = []
        for el in s:
            if el == modre:
                vrni.append(MODRA)
            elif el == zelene:
                vrni.append(ZELENA)
            elif el == rumene:
                vrni.append(RUMENA)
            elif el == rdece:
                vrni.append(RDECA)
        if vrni != []:
            return choice(vrni)
        return choice([MODRA, ZELENA, RUMENA, RDECA])

    def zaustavitvene_karte(self, roka):
        """Napad je najboljsa obramba!
        Vrnemo karte, ki so dobre za zaustavitev igralca pred nami, ce ima on v roki le eno karto.
        Indeksiranje: 0: skupaj, 1: vzemi4, 2: vzemi2, 3: stop, 4: obrni, 5: menjaj"""
        # menjaj smo dali pred obrni iz razloga, da se v najslabšem primeru lotimo obracanja smeri
        # zakaj bi to lahko bilo slabo?
        # morda ima igralec, ki bi bil naslednji na vrsti pri igranju v drugo smer v roki le eno karto
        vzemi4 = [karta for karta in roka if karta.tip == VZEMI4]
        vzemi2 = [karta for karta in roka if karta.tip == VZEMI2]
        stop = [karta for karta in roka if karta.tip == STOP]
        menjaj = [karta for karta in roka if karta.tip == MENJAJ]
        obrni = [karta for karta in roka if karta.tip == OBRNI]
        # na zadnjem mestu v seznamu imamo najbolj ucinkovito karto
        return obrni + menjaj + stop + vzemi2 + vzemi4


    #### IGRA (zanka) ####
    def igraj(self):
        """Zanka, ki vzpostavlja igro."""
        self.nova.config(state=DISABLED)
        if len(self.deck.karte) == 0:
            print('V igro smo dodali nov kupcek kart.')
            self.deck = Deck() # v igro damo nov kupcek kart
        
        if self.igralec_na_vrsti == IGRALEC:
            self.klikni_potezo()
            self.vzel_karto = False
            self.naprej.config(state=DISABLED)
        else:
            self.naredi_potezo(self.igralec_na_vrsti)

        indeks = (self.igralci.index(self.igralec_na_vrsti) + 1) % len(self.igralci)
        naslednji = self.igralci[indeks]
        
        if naslednji != IGRALEC or self.zadnja_karta.tip == STOP:
            if self.zadnja_karta.tip == STOP and self.igralci[(self.igralci.index(self.igralec_na_vrsti) + 2) % len(self.igralci)] == IGRALEC:
                self.zamik = 0
            else:
                self.zamik = 1500
        else:
            self.zamik = 0
            
        self.canvas.delete(self.tekst)
        barva = self.zadnja_karta.barva
        self.tekst = self.canvas.create_text(300, 350, text=barva, fill='white', font=('Batang', 16))
            
        # preverimo, ce imamo zmagovalca
        if self.imamo_zmagovalca(self.igralec_na_vrsti):
            self.statistika()
            self.nova.config(state=ACTIVE)
            if self.igralec_na_vrsti == IGRALEC:
                self.konec_tekst = self.canvas.create_text(400, 450, text='BRAVO! Zmagal/-a si. :)\nČe želiš igrati še naprej,\nklikni na gumb spodaj \'Nova igra\'.'.format(self.igralec_na_vrsti), fill='white', font=('Batang', 18))
            else:
                self.konec_tekst = self.canvas.create_text(400, 450, text='Zmagal je igralec: {}\nČe želiš igrati še naprej,\nklikni na gumb spodaj \'Nova igra\'.'.format(self.igralec_na_vrsti), fill='white', font=('Batang', 18))
            print('IGRE JE KONEC! Zmagovalec je: {0}'.format(self.igralec_na_vrsti))
            return

        # skok na naslednjega igralca
        if self.zadnja_karta.tip == STOP:
            if self.poteza == True:
                self.naslednji_igralec(self.igralec_na_vrsti, 2)
            else:
                self.naslednji_igralec(self.igralec_na_vrsti, 1)
        else:
            self.naslednji_igralec(self.igralec_na_vrsti, 1)

        if self.igralec_na_vrsti == IGRALEC:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(85, 600, image=self.trol)
            self.slikica.append(self.tr)
        if self.igralec_na_vrsti == RACUNALNIK1:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(135, 500, image=self.trol)
            self.slikica.append(self.tr)
        if self.igralec_na_vrsti == RACUNALNIK2:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(150, 150, image=self.trol)
            self.slikica.append(self.tr)
        if self.igralec_na_vrsti == RACUNALNIK3:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(660, 155, image=self.trol)
            self.slikica.append(self.tr)

        self.poteza = False # ponastavimo

        # nekaj moramo narediti se, ce se zgodi, da zmanjka kart na kupcku
        # self.odlozene premesamo in damo na kupcek
        # pri tem s canvasa pobrisemo vse karte na odlagalnem kupcku
        
        self.after_id = self.canvas.after(self.zamik, self.igraj)


    #### STATISTIKA ####
    def statistika(self):
        self.rac1t += self.tocke(self.hand[RACUNALNIK1])
        self.rac2t += self.tocke(self.hand[RACUNALNIK2])
        self.rac3t += self.tocke(self.hand[RACUNALNIK3])
        self.igrt += self.tocke(self.hand[IGRALEC])
        self.rac1.set(self.rac1t)
        self.rac2.set(self.rac2t)
        self.rac3.set(self.rac3t)
        self.igr.set(self.igrt)
        self.stevilo_iger += 1
        self.o.set(self.stevilo_iger)
        return

    def tocke(self, roka):
        vsota = 0
        for el in roka:
            vsota += el.vrednost
        return vsota
        

    #### NOVA IGRA ####
    def nova_igra(self, *args):
        print('*-.NOVA IGRA.-*')
        if self.after_id != None:
            self.canvas.after_cancel(self.after_id)
        self.after_id = None
        self.canvas.delete(ALL)
        self.odlozene = []
        self.slike = []
        self.deck = Deck()              # tukaj noter imam vse karte
        self.igralci = []               # seznam igralcev, kjer bomo določili, kdo igra prvi itd.
        self.hand = dict()              # slovar vseh igralcev in njihovih kart
        self.zadnja_karta = None        # da vemo, katera je zadnja karta, ki je padla na odlagalni kupcek
        self.igralec_na_vrsti = None    # na potezi
        self.vzel_karto = False         # pomozna spremenljivka za klikanje na kupcek
        self.odlozene = []              # karte, ki padejo na odlagalni kupcek
        self.var = BooleanVar(self.root)
        self.panika = False
        self.poteza = False             # preverjamo, ali je igralec odlozil kaksno karto ali ne
        self.slike_rac1 = []
        self.slike_rac2 = []
        self.slike_rac3 = []
        self.tekst = []
        self.slikica =[]


        #### GENERIRANJE IGRALCEV ####
        self.igralci = [IGRALEC, RACUNALNIK1, RACUNALNIK2, RACUNALNIK3]
        for igralec in self.igralci:
            self.hand[igralec] = []

        print(self.igralci)
        self.igralec_na_vrsti = self.igralci[randint(0,3)]
        print('Prvi bo na vrsti: ' + self.igralec_na_vrsti)

        # razdelimo karte
        self.deck.razdeli(self.hand, len(self.hand)*6)

        # dolociti moramo katera bo prva karta
            # tukaj je morda slabo, da igralci ne vidijo, katere karte iz premesanega
            # kupcka smo preskocili, ker niso bile ustrezne
        while True:
            karta = self.deck.karte.pop()
            if karta.tip == STOP or karta.tip == OBRNI or karta.tip == VZEMI2 or \
               karta.tip == VZEMI4 or karta.tip == MENJAJ:
                self.odlozene.append(karta)
            else:
                self.zadnja_karta = karta
                break


        #### IZRIS IGRALNE POVRSINE ####
        # kupcek, s katerega pobiramo karte
        self.kupcek = PhotoImage(file='./Karte/blank.gif')
        self.odlozene.append(self.kupcek)
        self.canvas.create_image(400, 300, image = self.kupcek)

        barva = self.zadnja_karta.barva
        self.tekst = self.canvas.create_text(300, 350, text=barva, fill='white', font=('Batang', 16))

        # odlagalni kupcek
        self.zadnja_karta.narisi(self.canvas, self.slovar_slik[(self.zadnja_karta.barva, self.zadnja_karta.tip)], 300, 300)
        self.odlozene.append(self.zadnja_karta) #ko je treba brisat, pobrisem vse, kar je tu notri ...

        # narisali bomo karte, ki jih ima v roki igralec
        self.slike = sorted(self.hand[IGRALEC])
        for (i, el) in enumerate(self.slike):
            el.narisi(self.canvas, self.slovar_slik[(el.barva, el.tip)], i*35 + 150, 600)

        # karte igralca racunalnik1
        self.vodoravna = PhotoImage(file='./Karte/blank_h.gif')
        for i in range(len(self.hand[RACUNALNIK1])):
            self.vod = self.canvas.create_image(62, 500 - i*35, image=self.vodoravna)
            self.slike_rac1.append(self.vod)

        # karte igralca racunalnik2
        self.navpicna = PhotoImage(file='./Karte/blank.gif')
        for i in range(len(self.hand[RACUNALNIK2])):
            self.nav = self.canvas.create_image(150 + i*35, 75, image=self.navpicna)
            self.slike_rac2.append(self.nav)

        # karte igralca racunalnik3
        self.vodoravna2 = PhotoImage(file='./Karte/blank_h.gif')
        for i in range(len(self.hand[RACUNALNIK3])):
            self.vod2 = self.canvas.create_image(730, 157 + i*35, image=self.vodoravna2)
            self.slike_rac3.append(self.vod2)

        # napisi igralcev
        self.ime1 = self.canvas.create_text(84, 538, text='Računalnik 1', fill='white', font=('Batang', 16))
        self.ime2 = self.canvas.create_text(180, 25, text='Računalnik 2', fill='white', font=('Batang', 16))
        self.ime3 = self.canvas.create_text(710, 118, text='Računalnik 3', fill='white', font=('Batang', 16))

        # trollface ^^
        if self.igralec_na_vrsti == RACUNALNIK1:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(135, 500, image=self.trol)
            self.slikica.append(self.tr)
        if self.igralec_na_vrsti == RACUNALNIK2:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(150, 150, image=self.trol)
            self.slikica.append(self.tr)
        if self.igralec_na_vrsti == RACUNALNIK3:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(660, 155, image=self.trol)
            self.slikica.append(self.tr)
        if self.igralec_na_vrsti == IGRALEC:
            self.trol = PhotoImage(file='./TrollFace.gif')
            self.tr = self.canvas.create_image(85, 600, image=self.trol)
            self.slikica.append(self.tr)

        
        #### ZAGON IGRE ####
        self.igraj()
        

#### ZAGON PROGRAMA ####
master = Tk()
aplikacija = CardGame(master, 4)
master.resizable(width=FALSE, height=FALSE) # uporabnik ne more spreminjati velikosti okna
master.mainloop()
        
        
